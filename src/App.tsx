import React from 'react';
import i18next from "i18next";
import './assets/css/style.scss'
import AddressComponent from './components/pikcha/address/addressComponent'
import {IAddress, Address} from "./shared/models/pikcha/addressModel";
import SmallestPositiveNumberComponent from "./components/pikcha/smallestPositiveNumber/smallestPositiveNumberComponent";
export interface IState {
    message: string,
    address: IAddress
}

export default class App extends React.Component<{}, IState>{
    constructor(props:any){
        super(props);
        this.state = {
            message:'',
            address: new Address()
        };
    }

    componentDidMount(){

    }
    componentDidUpdate(){

    }
    componentWillUnmount(){

    }
    changeLang(e:any){
        let val = e.currentTarget.value;
        i18next.changeLanguage(val).then(() => {
            i18next.options.lng = val;
            i18next.t('key');
            this.setState({
                message: ''
            })
        });
    }
    render(){
        return (
            <div className={'row'}>
                <select onChange={(e)=>{this.changeLang(e)}}>
                    <option value={'en'}>English</option>
                    <option value={'it'}>Italian</option>
                    <option value={'nl'}>Dutch</option>
                </select>
                <h1 className={'textCenter'}>{i18next.t('Components.welcomeToPikcha')}</h1>
                <div className={'row'}>
                    <SmallestPositiveNumberComponent></SmallestPositiveNumberComponent>
                </div>
                <hr/>
                <AddressComponent address={this.state.address}></AddressComponent>
            </div>

        );
    }
};
