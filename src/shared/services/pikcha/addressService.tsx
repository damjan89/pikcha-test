import BaseService from '../baseService';
import {IAddress} from 'shared/models/pikcha/addressModel';
const requestUrl = '/address';

export default class AddressService extends BaseService<IAddress> {
constructor (){
        super(requestUrl);
    }
};
