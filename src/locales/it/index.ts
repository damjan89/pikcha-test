import * as Buttons from './buttons.json'
import * as Components from './components.json'
import * as Pages from './pages.json'
export {Buttons, Components, Pages}
